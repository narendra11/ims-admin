const { collection } = require('forest-express-sequelize');
const {encrypt, decrypt} = require('../libs/crypto');

// This file allows you to add to your Forest UI:
// - Smart actions: https://docs.forestadmin.com/documentation/reference-guide/actions/create-and-manage-smart-actions
// - Smart fields: https://docs.forestadmin.com/documentation/reference-guide/fields/create-and-manage-smart-fields
// - Smart relationships: https://docs.forestadmin.com/documentation/reference-guide/relationships/create-a-smart-relationship
// - Smart segments: https://docs.forestadmin.com/documentation/reference-guide/segments/smart-segments
collection('companies', {
  actions: [],
  fields: [{
    field: 'password',
    type: 'String',
    get: (company) => {
      const encryptedPasswordComponents = {
        iv: company.encryptedPasswordIv,
        content: company.encryptedPassword,
      };
      return decrypt(encryptedPasswordComponents);
    },
    set: (company, password) => {
      const encryptedPassword = encrypt(password);
      company.encryptedPassword = encryptedPassword.content;
      company.encryptedPasswordIv = encryptedPassword.iv;

      return company;
    }
  }],
  segments: [],
});
